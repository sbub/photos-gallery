import React from "react";
import { useAppState } from "../hooks/useAppState";
import { Link } from "react-router-dom";

import { logout } from "../firebase";

const Header = () => {
  const [{ auth, authAttempted }] = useAppState();
  const [{ user }] = useAppState();

  return (
    <div>
      {authAttempted && auth ? (
        <button type="button" onClick={logout}>
          Logout
        </button>
      ) : (
        <Link to="/login">
          <p>Login</p>
        </Link>
      )}
      {user && <p>Welcome, {user.displayName ? user.displayName : "Human"}</p>}
    </div>
  );
};

export default Header;
