import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { login as signin, signup } from "../firebase";
import validateLogin from "../utils/validateLogin";
import useFormValidation from "../hooks/useFormValidation";

const INITIAL_STATE = {
  name: "",
  email: "",
  password: ""
};

const Login = ({ isLogin }) => {
  let history = useHistory();
  const [login, setLogin] = useState(isLogin);
  const [firebaseError, setFirebaseError] = useState(null);
  const {
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    errors,
    isSubmitting
  } = useFormValidation(INITIAL_STATE, validateLogin, authenticateUser);

  async function authenticateUser() {
    try {
      const { name, email, password } = values;
      login
        ? await signin(email, password)
        : await signup({ name, email, password });
      history.push("/");
    } catch (err) {
      console.error("Authentication error", err);
      setFirebaseError(err.message);
    }
  }

  const handleRouteRedirection = () => {
    history.push(login ? "/register" : "/login");
    setLogin(prevState => !prevState);
  };

  return (
    <div>
      <h2>{login ? "Login" : "Create Account"}</h2>
      <form onSubmit={handleSubmit}>
        {!login && (
          <input
            type="text"
            name="name"
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            placeholder="Your name"
            autoComplete="off"
          />
        )}
        <input
          type="email"
          name="email"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Your email"
          autoComplete="off"
        />
        {errors.email && <p>{errors.email}</p>}
        <input
          type="password"
          name="password"
          value={values.password}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Choose a secure password"
        />
        {errors.password && <p className="error-text">{errors.password}</p>}
        {firebaseError && <p className="error-text">{firebaseError}</p>}
        <button type="submit" disabled={isSubmitting}>
          Submit
        </button>
        <button type="button" onClick={handleRouteRedirection}>
          {login ? "need to create an account ?" : "already have an account ?"}
        </button>
      </form>
    </div>
  );
};

export default Login;
