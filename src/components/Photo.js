import React from "react";
import useFetch from "../hooks/useFetch";
import { constructUrl } from "../utils/api";
import Spinner from "./Spinner";
import Error from "./Error";

type Props = {
  id: string,
  title?: string
};

const cutLength = (word: string) => {
  return word.length > 17 ? `${word.slice(0, 17)}...` : word;
};

const Photo = (props: Props) => {
  const sizesUrl = constructUrl("getSizes", { photo_id: props.id });
  const {
    isLoading: isLoadingSizes,
    data: photoSizes,
    error: sizesError,
    setRetry
  } = useFetch(sizesUrl);

  const infoUrl = constructUrl("getInfo", { photo_id: props.id });
  const { isLoading: isLoadingInfo, data: info, error: infoError } = useFetch(
    infoUrl
  );

  let username: string, postedOn: Date, description: string;
  if (info && info.photo) {
    username = info.photo.owner.username;
    postedOn = new Date(parseInt(info.photo.dates.posted) * 1000);
    description = info.photo.description._content;
  }

  const renderCaptures = () => {
    return (
      <div className="photo-details">
        <p className="caption">{cutLength(username)}</p>
        <p className="caption">{postedOn.toUTCString()}</p>
        <p className="caption">{cutLength(description)}</p>
      </div>
    );
  };

  return (
    <div className="photo-container">
      {infoError || sizesError ? (
        <Error error={sizesError} onRetry={setRetry}>
          {(error, onRetry) => {
            return error === "Photo not found" ? (
              <p>{error}</p>
            ) : (
              <button onClick={() => onRetry(isRetry => !isRetry)}>
                Retry
              </button>
            );
          }}
        </Error>
      ) : (
        <>
          {isLoadingSizes && <Spinner />}
          {photoSizes && photoSizes.sizes && photoSizes.sizes.size && (
            <img
              className="photo-image"
              src={photoSizes.sizes.size[1].source}
              alt={props.title}
            />
          )}
          {isLoadingInfo && <Spinner />}
          {info && info.photo && renderCaptures()}
        </>
      )}
    </div>
  );
};

export default Photo;
