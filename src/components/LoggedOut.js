import React from "react";
import Gallery from "./Gallery";

const LoggedIn = () => <Gallery />;

export default LoggedIn;
