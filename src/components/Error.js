import * as React from "react";

type Props = {
  error: string | null,
  onRetry: any => void,
  children: (error: string | null, onRetry: (any) => void) => React.Node
};
const Error = ({ error, onRetry, children }: Props) => {
  return children(error, onRetry);
};

export default Error;
