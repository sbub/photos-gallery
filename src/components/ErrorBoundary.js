import * as React from "react";

type State = {
  hasError: boolean
};

type Props = {
  children: React.Node | null
};

class ErrorBoundary extends React.Component<Props, State> {
  state = {
    hasError: false
  };

  componentDidCatch() {
    this.setState({ hasError: true });
    // Additionally: Log the error to an error reporting service
  }

  render() {
    if (this.state.hasError) {
      return <p>An error occured</p>;
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
