import React, { useState, useEffect } from "react";

import Photo from "./Photo";
import { constructUrl } from "../utils/api";
import useFetch from "../hooks/useFetch";

const Gallery = () => {
  const [page, setPage] = useState(1);
  const [allPhotos, setAllPhotos] = useState([]);

  const url = constructUrl("search", { page, text: "dogs" });
  const { data } = useFetch(url);

  useEffect(() => {
    setAllPhotos(prevPhotos => {
      if (data && data.photos) {
        return [...prevPhotos, ...data.photos.photo];
      }
      return prevPhotos;
    });
  }, [data]);

  return (
    <div className="gallery-container">
      <div className="photos">
        {allPhotos.map(photo => (
          <Photo key={photo.id} id={photo.id} title={photo.title} />
        ))}
      </div>
      <button className="btn" onClick={() => setPage(page => page + 1)}>
        View more
      </button>
    </div>
  );
};

export default Gallery;
