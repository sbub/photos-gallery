const flickrAPI = "https://api.flickr.com/services/rest/";

if (!process.env.REACT_APP_FLICKR_KEY) throw new Error("Api Key is not found");
const apiKey: string = process.env.REACT_APP_FLICKR_KEY;

export const constructUrl = (method: string, queries: {} = {}) => {
  let queriesParsed;
  const keys = Object.keys(queries);

  if (keys.length) {
    queriesParsed = keys.reduce((acc, v) => {
      acc += `&${v}=${queries[v]}`;
      return acc;
    }, "");
  }
  return `${flickrAPI}?method=flickr.photos.${method}&api_key=${apiKey}${
    queriesParsed ? queriesParsed : ""
  }&format=json&nojsoncallback=1`;
};

export const partiallyGetUrl = (method: string) => (queries: {
  [key: string]: any
}) => fetch(constructUrl(method, queries)).then(res => res.json());

export const fetchPhotos = partiallyGetUrl("getRecent");
export const fetchPhotoInfo = partiallyGetUrl("getSizes");
