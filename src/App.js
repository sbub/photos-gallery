import React from "react";
import "./App.css";
import { AppStateProvider } from "./hooks/useAppState";
import appReducer, { initialState } from "./appReducer";
import useAuth from "./hooks/useAuth";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import ErrorBoundary from "./components/ErrorBoundary";
import LoggedIn from "./components/LoggedIn";
import LoggedOut from "./components/LoggedOut";
import Header from "./components/Header";
import Login from "./components/Login";

function App() {
  const { authAttempted, auth } = useAuth();
  if (!authAttempted) return null;
  return (
    <div>
      <Header />
      <Switch>
        <Route path="/login" render={() => <Login isLogin={true} />} />
        <Route path="/register" render={() => <Login isLogin={false} />} />
        <Route
          path="/"
          render={() => {
            return auth ? <LoggedIn /> : <LoggedOut />;
          }}
        />
      </Switch>
    </div>
  );
}

export default () => (
  <ErrorBoundary>
    <AppStateProvider reducer={appReducer} initialState={initialState}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </AppStateProvider>
  </ErrorBoundary>
);
