import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

import firebaseConfig from "./config";

app.initializeApp(firebaseConfig);

export const db = app.firestore();

export const auth = () => app.auth();
