import { auth } from "./firebase";

export const onAuthStateChanged = (callback: (auth: {} | null) => void) => {
  auth().onAuthStateChanged(callback);
};

export async function signup({
  email,
  password,
  name
}: {
  [key: string]: string
}) {
  try {
    const { user } = await auth().createUserWithEmailAndPassword(
      email,
      password
    );
    await user.updateProfile({
      displayName: name
    });
  } catch (e) {
    throw e;
  }
}

export async function login(email, password) {
  return await auth().signInWithEmailAndPassword(email, password);
}

export async function logout() {
  await auth().signOut();
}
