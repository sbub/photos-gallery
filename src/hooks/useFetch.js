import { useEffect, useReducer, useState } from "react";

const REQUEST_START = "REQUEST_START";
const REQUEST_SUCCESS = "REQUEST_SUCCESS";
const REQUEST_ERROR = "REQUEST_ERROR";

const reducer = (state, action) => {
  switch (action.type) {
    case REQUEST_START:
      return {
        ...state,
        isLoading: true
      };
    case REQUEST_SUCCESS:
      return {
        isLoading: false,
        data: action.data,
        error: null
      };
    case REQUEST_ERROR:
      return {
        isLoading: false,
        data: null,
        error: action.error
      };
    default:
      return state;
  }
};
const initialState = {
  isLoading: false,
  data: null,
  error: null
};

const useFetch = (url: string) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [retry, setRetry]: [
    boolean,
    (boolean | (boolean => boolean)) => void
  ] = useState(false);

  useEffect(() => {
    const abortController = new AbortController();

    const fetchData = async () => {
      dispatch({ type: REQUEST_START });
      try {
        const response = await fetch(url, { signal: abortController.signal });
        const data = await response.json();

        if (data.stat !== "ok") {
          dispatch({ type: REQUEST_ERROR, error: data.message });
        } else {
          dispatch({ type: REQUEST_SUCCESS, data });
        }
      } catch (err) {
        console.error(`Error while fetching data with ${url}`, err);
        dispatch({ type: REQUEST_ERROR, error: err.message });
      }
    };

    fetchData();

    return () => {
      abortController.abort();
    };
  }, [url, retry]);

  return { ...state, setRetry };
};

export default useFetch;
