import * as React from "react";

type State = {
  authAttempted: boolean,
  auth: null | {},
  user: null | {}
};

type Action = {
  type: "AUTH_CHANGE" | "LOAD_USER"
};

type Dispatch = Action => void;

type Children = React.Node | null;

const Context: {
  Consumer: any,
  Provider: any,
  displayName?: string
} = React.createContext();

export function AppStateProvider({
  reducer,
  initialState = {},
  children
}: {
  reducer: (State, Action) => State,
  initialState: State,
  children: Children
}) {
  const value: [State, Dispatch] = React.useReducer(reducer, initialState);
  return <Context.Provider value={value} children={children} />;
}

export function useAppState() {
  return React.useContext(Context); // getting [appState, dispatch]
}
