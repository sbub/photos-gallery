import { useEffect } from "react";
import { useAppState } from "./useAppState";
import { onAuthStateChanged } from "../firebase";

type Dispatch = ({
  type: "AUTH_CHANGE" | "LOAD_USER",
  auth?: null | {},
  user?: null | {}
}) => void;

export default function useAuth() {
  const [
    { authAttempted, auth, user }: { authAttempted: boolean, auth: {} | null },
    dispatch: Dispatch
  ] = useAppState();

  useEffect(() => {
    return onAuthStateChanged(auth => {
      if (auth) {
        const { displayName, email } = auth;
        console.log("running useAuth");

        dispatch({
          type: "AUTH_CHANGE",
          auth: {},
          user: {
            displayName,
            email
          }
        });
      } else {
        dispatch({
          type: "AUTH_CHANGE",
          auth: null
        });
      }
    });
  }, [dispatch]);

  return { authAttempted, auth, user };
}
