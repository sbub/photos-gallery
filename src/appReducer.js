const initialState = {
  authAttempted: false,
  auth: null,
  user: null
};

const appStateReducer = (
  state: { [key: string]: any },
  action: { [key: string]: any }
): { [key: string]: any } => {
  switch (action.type) {
    case "AUTH_CHANGE": {
      return {
        ...state,
        auth: action.auth,
        authAttempted: true,
        user: action.user
      };
    }
    default:
      return state;
  }
};

export { initialState, appStateReducer as default };
